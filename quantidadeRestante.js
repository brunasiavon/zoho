// Header
quantity = input.QUANTIDADE_TONELADAS;
pedido = invokeUrl
[
    url: "https://www.zohoapis.com/crm/v6/Sales_Orders/" + input.ID_do_Pedido
    type: GET
    connection: "crmall"
];
pedido = pedido.get("data").get(0);
// Parsing
for each product in pedido.get("Ordered_Items") {
        if (quantity > product.get("quantidadeRestante")) {
            alert "Você não possui saldo o suficiente para completar o pedido, Saldo restante: "+product.get("quantidadeRestante");
            input.QUANTIDADE_TONELADAS = null;
        }
}