// console.clear()

const user = $Crm.user.id;

const cep = ZDK.Page.getField("zPlugin_CEP").getValue();
const cepFormatter = cep.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
if (cepFormatter.length <= 7 && cepFormatter.length > 8) return ZDK.Client.showAlert("CEP inválido.");

if (cepFormatter.length === 8) {
    const response = cepConsult(cepFormatter);
    if (response.cep == null) {
        return ZDK.Client.showAlert("CEP inválido.");
    }
    const userInfo = getUserInfo(user);

    const setCep = ZDK.Page.getField("zPlugin_CEP");
    setCep.setValue(response.cep ? response.cep.toUpperCase() : null);
    if (userInfo !== "Administrator" && setCep.getValue() != "") {
        setCep.setReadOnly(true);
    }

    const logradouro = response.logradouro ? response.logradouro.toUpperCase() : null;
    const bairro = response.bairro ? response.bairro.toUpperCase() : null;
    // Concatenar logradouro e bairro com ", ," se ambos existirem
    const streetValue = (logradouro && bairro) ? logradouro + ", ," + bairro : (logradouro || bairro);
    // Definir o valor do campo Street
    const streetField = ZDK.Page.getField("Billing_Street");
    streetField.setValue(streetValue);
    if (userInfo !== "Administrator" && streetField.getValue() != "") {
    streetField.setReadOnly(true);
    }

    const localidade = ZDK.Page.getField("Billing_City");
    localidade.setValue(response.localidade ? response.localidade.toUpperCase() : null);
    if (userInfo !== "Administrator" && localidade.getValue() != "") {
        localidade.setReadOnly(true);
    }

    const uf = ZDK.Page.getField("Billing_State");
    uf.setValue(response.uf ? response.uf.toUpperCase() : null);
    if (userInfo !== "Administrator") {
        uf.setReadOnly(true);
    }

    ZDK.Page.getField("zPlugin_Complemento").setValue(response.complemento ? response.complemento.toUpperCase() : null);
}

function cepConsult(cep) {
    try {
        const getReturnMessage = (invokeResult) => invokeResult.details.output;
        const functionResult = ZDK.Apps.CRM.Functions.execute("consult_via_cep", {
            cep: cep
        });

        const returnMap = getReturnMessage(functionResult);
        const response = JSON.parse(returnMap);
        return response;
    } catch (err) {
        console.log(err.message);
        ZDK.Client.showAlert("Não foi possível consultar o CEP.");
    }
}

function getUserInfo(user) {
    try {
        const functionUserInfo = ZDK.Apps.CRM.Functions.execute("consult_role", {
            userID: user
        });
        const userInfo = functionUserInfo._details.output;
        console.log(userInfo);
        return userInfo;
    } catch (err) {
        console.log(err);
    }
}